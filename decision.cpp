#include "decision.h"
#include "Consts.h"
#include <omp.h>


void DECISION::initialize() {
   for(int i = 0; i < CNT_DX; i++) { //segment
       for(int j = 0; j < CNT_BAS; j++) { //basis koef
           double sum = 0;
           for(int k = 0; k < CNT_BAS; k++) { // * MATRIX
               sum += M->MATRIX_REV(j, k) * integral(k, i);
           } 
           y[i][j] = sum;
           y[i][j] /= DX;      
           
            
       }       
   }
} 



    
int DECISION:: updateDec(int type) {
   
     this->remove();


     this->setDec(type); 
}
   





double DECISION::getFullDec(double x) {
    double eps = 10e-15;
    for(int i = 0; i < CNT_DX; i++) {
        if(fabs(x - i * DX) < eps) {
            return this->getDecBounds(i, x);
        } else {
            if( ((x  -  i * DX) > eps) && (((i + 1) * DX - x) > eps)) {
                return this->getDecInnary(i, x);
            }
        }
    }
}

double DECISION::getPowY(int i, int k, double x) {
     return getFullDec(x) * getFullDec(x) * getFi(i, k, x); 
}



double DECISION::getIntegral(double a, double b, int ii, int CNT_BAS) {
    double integral = 0;
    double t[5];
    double c[5];
    t[0] = 0.906179845938664;
    t[1] = 0.538469310105683;
    t[2] = 0.0;
    t[3] = -t[1];
    t[4] = -t[0];
    c[0] = 0.236926885056189;
    c[1] = 0.478628670499366;
    c[2] = 0.568888888888889;
    c[3] = c[1];
    c[4] = c[0];
    double step = (b - a) / 100.0;
    
    for(int i = 0; i < 100; i++) {
        double x1 = a + i * step;
        double x2 = a + (i + 1) * step;
         double integral2 = 0;


        for(int j = 0; j < 5; j++) {
            double x = (x2 - x1) * (t[j] + 1) / 2.0 + x1;
            integral2 += c[j] * getPowY(ii, CNT_BAS, x);
        }
        integral += integral2 * (x2 - x1) / 2;

    }


    return integral;
}


double DECISION::getDecInnary(int kk, double x) {
    double sum = 0;

  
    
   
   
    for(int i = 0; i < CNT_BAS; i++) {
        sum += y[kk][i] * getFi(i, kk, x); 
    }
    return sum;
   
}


double DECISION::getDecInnary2(int k, double x) {
    double sum = 0;
    for(int i = 0; i < CNT_BAS; i++) {
        sum += y_prev[k][i] * getFi(i, k, x); 
    }
    return sum;
}

/*
double DECISION::getDecBounds(int k, double x, int pType) {
    double CU; //Custom constant
    int lType;
    if(pType == -1) { 
        lType = TYPE_FLUX;
    } else {
        lType = pType;
    } 
  
    switch(lType) {
    case(0):
        switch(type) {
        
        case(0): // for U
             if(fabs(x  - B) < 10e-3) {
                 return getDecInnary(0, A); 
                //return fNewVis(B, T);
             } else {
                return getDecInnary(k, x);
             }
             break;
        case(1):
            if(x == A) {
                return getDecInnary(CNT_DX - 1, B);
            } else {
                return getDecInnary(k - 1, x);
            }
            break;
        }
        break; 
      


    case(1):
        switch(type) {
        
        case(1): // for U
             if(x == B) {
                 return getDecInnary(0, A); 
                //return fNewVis(B, T);
             } else {
                /*if(x == A) {
                    return fNewVis(A, T);
                }
                return getDecInnary(k, x);
             }
             break;
        case(0):
            if(x == A) {
                return getDecInnary(CNT_DX - 1, B);
            } else {
                return getDecInnary(k - 1, x);
            }
            break;
        }
        break;
    
    case(2):
        //(U- + U+) / 2
        if((x == A) || (x == B)) {
            return  (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2;
         
        } else {
            return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2;
        }
        break;
  
   case(3): 
        switch(type) {
        case(0): 
            if((x == A) || (x == B)) {
                return (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2;
            } else {
                return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2; 
            }
            break;
       case(1):
          if((x == A) || (x == B)) {
              return  (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2 + CK * (-getDecInnary2(CNT_DX - 1, B) + getDecInnary2(0, A));
          } else {
              return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2 + CK * (getDecInnary2(k, x) - getDecInnary2(k - 1, x));
          }
          break; 
      }
   case(4): 
        switch(type) {
        case(0): 
            if((x == A) || (x == B)) {
                return (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2;
            } else {
                return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2; 
            }
            break;
       case(1):
          if((x == A) || (x == B)) {
              CU = fabs((getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / (fabs(getDecInnary2(CNT_DX - 1, B)) + fabs(getDecInnary2(0, A))));
              
              
               
               return  (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2 + CU * (-getDecInnary2(CNT_DX - 1, B) + getDecInnary2(0, A));
          } else {
              CU = fabs((getDecInnary(k - 1, x) + getDecInnary(k, x)) / (fabs(getDecInnary2(k, x)) + fabs(getDecInnary2(k - 1, x))));

              
              return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2 + CU * (getDecInnary2(k, x) - getDecInnary2(k - 1, x));
          }
          break; 
      }
   } 
      
}*/

double DECISION::getDecBounds(int kk, double xx, int pType) {
    double CU; //Custom constant
    int lType;
    if(pType == -1) { 
        lType = TYPE_FLUX;
    } else {
        lType = pType;
    } 
  
    
    int k = kk;
    double x = xx;    
  
    
    /*
    if (k == -1) {
        k = CNT_DX;
        x = B; 
    }

   if (k == CNT_DX + 1) {
        k = 0;
        x = A; 
   }*/
    
    
    switch(lType) {
 
    case(0):
        switch(type) {
        
        case(0): // for U
             if(fabs(x  - B) < 10e-3) {
                 return getDecInnary(0, A); 
                //return fNewVis(B, T);
             } else {
                
                return getDecInnary(k, x);
             }
             break;
        case(1):
            if(x == A) {
                return getDecInnary(CNT_DX - 1, B);
            } else {
                return getDecInnary(k - 1, x);
            }
            break;
        }
        break; 
      


    case(1):
        switch(type) {
        
        case(1): // for U
             if(x == B) {
                 return getDecInnary(0, A); 
                //return fNewVis(B, T);
             } else {
                /*if(x == A) {
                    return fNewVis(A, T);
                }*/
                return getDecInnary(k, x);
             }
             break;
        case(0):
            if(x == A) {
                return getDecInnary(CNT_DX - 1, B);
            } else {
                return getDecInnary(k - 1, x);
            }
            break;
        }
        break;
    
    case(2):
        //(U- + U+) / 2
        if((x == A) || (x == B)) {
            return  (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2;
         
        } else {
            return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2;
        }
        break;
  
   case(3): 
        switch(type) {
        case(0): 
            if((x == A) || (x == B)) {
                return (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2;
            } else {
                return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2; 
            }
            break;
       case(1):
          if((x == A) || (x == B)) {
              return  (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2 + CK * (-getDecInnary2(CNT_DX - 1, B) + getDecInnary2(0, A));
          } else {
              return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2 + CK * (getDecInnary2(k, x) - getDecInnary2(k - 1, x));
          }
          break; 
      }
   case(4): 
        switch(type) {
        case(0): 
            if((x == A) || (x == B)) {
                return (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2;
            } else {
                return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2; 
            }
            break;
       case(1):
          if((x == A) || (x == B)) {
              CU = fabs((getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / (fabs(getDecInnary2(CNT_DX - 1, B)) + fabs(getDecInnary2(0, A))));
              CU *= CK;
              return  (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2 + CU * (-getDecInnary2(CNT_DX - 1, B) + getDecInnary2(0, A));
          } else {
              CU = fabs((getDecInnary(k - 1, x) + getDecInnary(k, x)) / (fabs(getDecInnary2(k, x)) + fabs(getDecInnary2(k - 1, x))));

              CU *= CK;
              return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2 + CU * (getDecInnary2(k, x) - getDecInnary2(k - 1, x));
          }
          break; 
      }
   case(5):
       switch(type) {
        case(0): 
            if((x == A) || (x == B)) {
                return (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2;
            } else {
                return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2; 
            }
            break;
       case(1):
          if((x == A) || (x == B)) {
              CU = fabs(    ( getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A) ) 
                            / 
                            (
                             fabs (getDecInnary2(CNT_DX - 1, B) ) + fabs( getDecInnary2(0, A) )
                             + DX * ( 
                                         fabs (getDecInnary(CNT_DX - 1, B)) + fabs(getDecInnary(0, A))        
                                    ) 
                            )
                       );
              CU = CU * CK;       
              
              return  (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2 + CU * (-getDecInnary2(CNT_DX - 1, B) + getDecInnary2(0, A));
          } else {
              CU = fabs(   ( getDecInnary(k - 1, x) + getDecInnary(k, x) ) / 
                           (
                               fabs(getDecInnary2(k, x)) + fabs(getDecInnary2(k - 1, x))
                            
                            + DX * (
                                     fabs(getDecInnary(k - 1, x)) + fabs(getDecInnary(k, x))
                                   ) 
                           )
                        );
                      
               CU = CU * CK;           

             
              return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2 + CU * (getDecInnary2(k, x) - getDecInnary2(k - 1, x));
          }
          break; 
      }
   case(6):
         
       switch(type) {
        case(0): 
            if((x == A) || (x == B)) {
                
              
                return (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2;
            } else {
                
              
                return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2; 
            }
            break;
       case(1):
          if((x == A) || (x == B)) {
              double CU1 =  ( fabs(getDecInnary(CNT_DX - 1, B)) + fabs(getDecInnary(0, A)));
              double CU2 =  (
                             fabs (getDecInnary2(CNT_DX - 1, B) ) + fabs( getDecInnary2(0, A) )
                             + DX * ( 
                                         fabs (getDecInnary(CNT_DX - 1, B)) + fabs(getDecInnary(0, A))        
                                    ) 
                            );
              CU = CK * fabs(   CU1
                            / 
                           CU2
                       );
                     
                /*
                std::ofstream outL1("Const.txt", std::ios::app); 
                outL1 <<   CU1   << " "
                    <<   CU2  << " " << CU /CK << std::endl;
              outL1.close();  
             */
              return  (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2 + CU * (-getDecInnary2(CNT_DX - 1, B) + getDecInnary2(0, A));
          } else {
               double CU1 = ( fabs(getDecInnary(k - 1, x)) + fabs(getDecInnary(k, x)) );
               double CU2 =  (
                               fabs(getDecInnary2(k, x)) + fabs(getDecInnary2(k - 1, x))
                            
                            + DX * (
                                     fabs(getDecInnary(k - 1, x)) + fabs(getDecInnary(k, x))
                                   ) 
                           );
               CU = CK * fabs(  CU1 / 
                          CU2
                        );
               
              /*
              std::ofstream outL1("Const.txt", std::ios::app); 
              outL1 <<   CU1   << " "
                    <<   CU2  << " " << CU / CK << std::endl;
              outL1.close();  
                */
             
              return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2 + CU * (getDecInnary2(k, x) - getDecInnary2(k - 1, x));
          }
        
          break; 
      }
    case(7):
       switch(type) {
        case(0): 
            if((x == A) || (x == B)) {
                return (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2;
            } else {
                return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2; 
            }
            break;
       case(1):
          if((x == A) || (x == B)) {
              CU = fabs(    ( fabs(getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) ) 
                            / 
                            (
                             fabs (getDecInnary2(CNT_DX - 1, B) ) + fabs( getDecInnary2(0, A) )
                             + DX * ( 
                                         fabs (getDecInnary(CNT_DX - 1, B)) + fabs(getDecInnary(0, A))        
                                    ) 
                            )
                       );
               CU = CU * CK;        
              
              return  (getDecInnary(CNT_DX - 1, B) + getDecInnary(0, A)) / 2 + CU * (-getDecInnary2(CNT_DX - 1, B) + getDecInnary2(0, A));
          } else {
              CU = fabs(   ( fabs(getDecInnary(k - 1, x) + getDecInnary(k, x)) ) / 
                           (
                               fabs(getDecInnary2(k, x)) + fabs(getDecInnary2(k - 1, x))
                            
                            + DX * (
                                     fabs(getDecInnary(k - 1, x)) + fabs(getDecInnary(k, x))
                                   ) 
                           )
                        );
                      
                       
                CU = CU * CK;  
             
              return  (getDecInnary(k - 1, x) + getDecInnary(k, x)) / 2 + CU * (getDecInnary2(k, x) - getDecInnary2(k - 1, x));
          }
          break; 
      }

}
}



void DECISION::copy(double **r) {
    for(int i = 0; i < CNT_DX; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            y[i][j] = r[i][j];
        }
    }
}

void DECISION::copy_prev() {
    for(int i = 0; i < CNT_DX; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            y_prev[i][j] = y[i][j];
        }
    }
}


 

      

//Get derivation from U

void DECISION::getP(DECISION &P) {
      

    double start = omp_get_wtime();
    
        
    #pragma omp parallel for
    for(int i = 0; i < CNT_DX; i++) { //segment iter
        //double *r = new double[CNT_BAS];
        std::vector<double> r(CNT_BAS);
        double x2 =  A + (i + 1) * DX;
        double x1 =  A + i * DX;
         
        #pragma omp parallel for
        for(int j = 0; j < CNT_BAS; j++) { //basis koef
            double v = 0;
            for(int k = 0; k < CNT_BAS; k++) {
                v += y[i][k] * M->MATRIX_DER(j, k);
            }
               
            r[j] =  getDecBounds(i + 1, x2) * getFi(j, i, x2) - getDecBounds(i, x1) * getFi(j, i, x1) - v;
            
        }
        
        #pragma omp parallel for              
        for(int j = 0; j < CNT_BAS; j++) {
           double v = 0; 
           for(int k = 0; k < CNT_BAS; k++) {
               v += r[k] * M->MATRIX_REV(j, k);
           }   
           P.y[i][j] = v / DX;
           
        }
        //delete[] r;
    }
    
   
}

//
/*double multVects(double *y, double *z, int num) {
    double *k = new double[6];          //
    k[0] = y[0] * z[0];                 //1
    k[1] = y[1] * z[0] + z[1] * y[0];   //x
    k[2] = y[2] * z[0] + z[2] * y[0];   //x^2
    k[3] = y[2] * z[1] + z[2] * y[1];   //x^3
    k[4] = y[1] * z[1];                 //x^2
    k[5] = y[2] * z[2];                 //x^4


    double sum = 0;
    for(int i = 0; i < 6; i++) {
        sum += DD[num][i] * k[i] * DX;
    }
    delete[] k;
    return sum;
}


double multVectsDer(double *y, double *z, int num) {
    double *k = new double[6];          //
    k[0] = y[0] * z[0];                 //1
    k[1] = y[1] * z[0] + z[1] * y[0];   //x
    k[2] = y[2] * z[0] + z[2] * y[0];   //x^2
    k[3] = y[2] * z[1] + z[2] * y[1];   //x^3
    k[4] = y[1] * z[1];                 //x^2
    k[5] = y[2] * z[2];                 //x^4


    double sum = 0;
    for(int i = 0; i < 6; i++) {
        sum += CC[num][i] * k[i];
    }
    delete[] k;
    return sum;
}


//dUdt
double** DECISION::getdUdTBurgers1(DECISION &P) {
    double r0, r1, r2;
    double **d;
    d = new double*[CNT_DX];
    for(int i = 0; i < CNT_DX; i++) {
       d[i] = new double[3];

    }
    for(int i = 0; i < CNT_DX; i++) {
       double x2 =  A + (i + 1) * DX;
       double x1 =  A + i * DX;

       r0 = P.getDecBounds(i + 1, x2) - P.getDecBounds(i, x1) - multVects(y[i], P.y[i], 0);
       r1 = P.getDecBounds(i + 1, x2) * fi1(i, x2) - P.getDecBounds(i, x1) * fi1(i, x1) -
            (P.y[i][0] * C[1][0] + P.y[i][1] * C[1][1] + P.y[i][2] * C[1][2]) - multVects(y[i], P.y[i], 1);

       r2 = P.getDecBounds(i + 1, x2) * fi2(i, x2) - P.getDecBounds(i, x1) * fi2(i, x1) -
              (P.y[i][0] * C[2][0] + P.y[i][1] * C[2][1] + P.y[i][2] * C[2][2]) - multVects(y[i], P.y[i], 2);

       switch(CNT_BAS) {
       case(1):
           r1 = 0;
           r2 = 0;
           break;
       case(2):
           r2 = 0;
           break;
       }
       switch(CNT_BAS) {
       case(3):
           d[i][0] = Areverse2[0][0] * r0 + Areverse2[0][1] * r1 + Areverse2[0][2] * r2;
           d[i][1] = Areverse2[1][0] * r0 + Areverse2[1][1] * r1 + Areverse2[1][2] * r2;
           d[i][2] = Areverse2[2][0] * r0 + Areverse2[2][1] * r1 + Areverse2[2][2] * r2;
           break;
       case(2):
           d[i][0] = Areverse1[0][0] * r0 + Areverse1[0][1] * r1;
           d[i][1] = Areverse1[1][0] * r0 + Areverse1[1][1] * r1;
           d[i][2] = 0;
           break;
       case(1):
           d[i][0] =ё r0;
           d[i][1] = 0;
           d[i][2] = 0;
           break;
       }
       d[ico][0] = d[i][0] / DX;
       d[i][1] = d[i][1] / DX;
       d[i][2] = d[i][2] / DX;
    }

    return d;
}*/


void DECISION::export2gnuplot(const char * file_name, double t) {
       std::ofstream outGraph(file_name, std::ios::trunc);    //Output graph

               double h = DX / CNT_PNT_SEG;
               for(int l = 0; l <= CNT_DX; l++) {
                   for(int j = 0; j  < CNT_PNT_SEG; j++) {
                       if((l == CNT_DX) && (j > 0)) {
                           break;
                       }
                       double x = A + l * DX + j * h;
                       double U;
                       double UP = getPreciseDec(x, t);
                       if(j == 0) {
                           U = this->getDecBounds(l, x);
                       } else {
                           U = this->getDecInnary(l, x);
                       }
                       //std::cout << x << " " << U << " " << UP  << std::endl;
                       outGraph << x << " " << U << " " << UP  << std::endl;
               
                   }
               }
              outGraph.close();
}


void DECISION::save_file(int num_step) {
    std::ofstream savefile("file_save.txt", std::ios::trunc);
    
    savefile  << T << " " << CNT_BAS << " " << CNT_DX << " " << num_step <<  std::endl;
  


    for(int i = 0; i < CNT_DX; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            savefile << y[i][j] << " ";
        } 
        savefile << std::endl; 
    }  

    savefile.close();
} 





void DECISION::recover_dec() {
    std::ifstream savefile("file_save.txt");
    savefile  >> T >> CNT_BAS >> CNT_DX;
    


    for(int i = 0; i < CNT_DX; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            savefile >> y[i][j];
        } 
    }          

  


}

// dUdt
// 
double** DECISION::getdUdTBurgers2(DECISION &P) {
    double **res;  
    double nu =  NU;
    res = new double*[CNT_DX];

    for(int i = 0; i < CNT_DX; i++) {
        res[i] = new double[CNT_BAS];
    }
    
    double start = omp_get_wtime();
    
    //#pragma omp parallel for
    for(int i = 0; i < CNT_DX; i++) {

        std::vector<double> r(CNT_BAS);
        double x2 =  A + (i + 1) * DX;
        double x1 =  A + i * DX;

        //#pragma omp parallel for
        for(int j = 0; j < CNT_BAS; j++) { //basis koef
            double v1 = 0;
            double v2 = 0;
            for(int k = 0; k < CNT_BAS; k++) {
                v1 += P.y[i][k] * M->MATRIX_DER(j, k);

            }
             
               

            for(int k = 0; k < CNT_BAS; k++) {
                for(int m = 0; m < CNT_BAS; m++) {
                    v2 += 0.5 * y[i][k] * y[i][m] * M->MATRIX_3DER(k, m, j);
                }
            }
            double C1, C2;
            if(i == 0) {
                C2 = fmax(fabs(getDecInnary(i, x1)), fabs(getDecInnary(CNT_DX - 1, B)));
            } else {
                C2 = fmax(fabs(getDecInnary(i, x1)), fabs(getDecInnary(i - 1, x1)));
            }
        
            if(i == CNT_DX -1) {
                C1 = fmax(fabs(getDecInnary(i, x2)), fabs(getDecInnary(0, A)));
            } else {
                C1 = fmax(fabs(getDecInnary(i + 1, x2)), fabs(getDecInnary(i, x2)));
            }
               
           
            
            double FR, FL; 

            if(i == CNT_DX -1) {
                //FR = 0;
                FR =   0.5 * (getDecInnary(0, A) * getDecInnary(0, A)
                          +  getDecInnary(i, x2) 
                         * getDecInnary(i, x2)) / 2;  
                          - C1 *   (getDecInnary(0, A) - getDecInnary(i, x2)) / 2; 
            } else {
                FR =   0.5 * (getDecInnary(i + 1, x2) * getDecInnary(i + 1, x2)
                            +  getDecInnary(i, x2) * getDecInnary(i, x2)) / 2 ; 
                           - C1 *   (getDecInnary(i + 1, x2) - getDecInnary(i, x2)) / 2;
            }

            if(i == 0) {
                //FL = 0;
                FL =   0.5 * (getDecInnary(i, x1) * getDecInnary(i, x1)
                           +  getDecInnary(CNT_DX - 1, B) * getDecInnary(CNT_DX - 1, B)) / 2; 
                           - C2 *  (getDecInnary(i, x1) - getDecInnary(CNT_DX - 1, B)) / 2; 
            } else {
                  FL =   0.5 * (getDecInnary(i, x1) * getDecInnary(i, x1) 
                           +  getDecInnary(i - 1, x1) * getDecInnary(i - 1, x1)) / 2 ; 
                          - C2 *  (getDecInnary(i, x1) - getDecInnary(i - 1, x1)) / 2; 
                
            }
            /*
            if(i == 0) {
                FL = 0;
            }

            if(i == CNT_DX -1) {
                FR = 0;
            }*/
 
        
            r[j] =  nu *  (P.getDecBounds(i + 1, x2) * getFi(j, i, x2) - P.getDecBounds(i, x1) * getFi(j, i, x1) - v1)
                    -(FR * getFi(j, i, x2) - FL * getFi(j, i, x1)) + v2;   

             
         }
         
         for(int j = 0; j < CNT_BAS; j++) {
            double v = 0; 
            for(int k = 0; k < CNT_BAS; k++) {
               v += r[k] * M->MATRIX_REV(j, k);
            }   
            res[i][j] = v / DX; 
        }  
        std::vector<double>().swap(r);
   }
  
   return res;

}


double** DECISION::getdUdtTransport(DECISION &P) {
    double **d = new double*[CNT_DX];
    for(int i = 0; i < CNT_DX; i++) {
        d[i] = new double[CNT_BAS];
    }
    
    //getP(P);
    for(int i = 0; i < CNT_DX; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            d[i][j] = P.y[i][j];
           
        }
    }
    return d;
}







double** DECISION::getdUdTHeat(DECISION &P) {
    getP(P);     

    double start = omp_get_wtime();
 
    double **res;  
    res = new double*[CNT_DX];

    for(int i = 0; i < CNT_DX; i++) {
        res[i] = new double[CNT_BAS];
    }  
    
 
    #pragma omp parallel for 
    for(int i = 0; i < CNT_DX; i++) {
        //double *r = new double[CNT_BAS];
        std::vector<double> r(CNT_BAS);
        double x2 =  A + (i + 1) * DX;
        double x1 =  A + i * DX;
        // #pragma omp parallel for
        for(int j = 0; j < CNT_BAS; j++) { //basis koef
            double v = 0;
            for(int k = 0; k < CNT_BAS; k++) {
                v += P.y[i][k] * M->MATRIX_DER(j, k);
            }
            r[j] =  P.getDecBounds(i + 1, x2) * getFi(j, i, x2) - P.getDecBounds(i, x1) * getFi(j, i, x1) - v;  
            
           
        }
        //#pragma omp parallel for
        for(int j = 0; j < CNT_BAS; j++) {
            double v = 0; 
            for(int k = 0; k < CNT_BAS; k++) {
               v += r[k] * M->MATRIX_REV(j, k);
            }   
            res[i][j] = v / DX; 
            
        } 
        //delete[] r;
    }
    return res;
}  

 
/********************************
*   Convection with viscosity   *
*********************************/  

double** DECISION::getdUdTViscos(DECISION &P) {
    double **res;  
    res = new double*[CNT_DX];

    for(int i = 0; i < CNT_DX; i++) {
        res[i] = new double[CNT_BAS];
    }
    
    double start = omp_get_wtime();
    
    #pragma omp parallel for
    for(int i = 0; i < CNT_DX; i++) {
        std::vector<double> r(CNT_BAS);
        double x2 =  A + (i + 1) * DX;
        double x1 =  A + i * DX;

        #pragma omp parallel for
        for(int j = 0; j < CNT_BAS; j++) { //basis koef
            double v1 = 0;
            double v2 = 0;
            for(int k = 0; k < CNT_BAS; k++) {
                v1 += P.y[i][k] * M->MATRIX_DER(j, k);
                v2 += y[i][k] * M->MATRIX_DER(j, k);
            }
            r[j] =  0.5 * P.getDecBounds(i + 1, x2) * getFi(j, i, x2) - 0.5 * P.getDecBounds(i, x1) * getFi(j, i, x1) - v1
                    -(getDecBounds(i + 1, x2) * getFi(j, i, x2) - getDecBounds(i, x1) * getFi(j, i, x1)) + v2;  
         }
    
         for(int j = 0; j < CNT_BAS; j++) {
            double v = 0; 
            for(int k = 0; k < CNT_BAS; k++) {
               v += r[k] * M->MATRIX_REV(j, k);
            }   
            res[i][j] = v / DX; 
        }   
        std::vector<double>().swap(r);

   }
   return res;
}

void DECISION::getDUDT(int num_method, DECISION &P, double** &DUDT) {
    switch(num_method) {
    case(0):
       DUDT = getdUdTViscos(P);
       break;
    case(1):
       //DUDT = getdUdTBurgers1(P);
       break;
    case(2):
       DUDT = getdUdTBurgers2(P);
       break;
    case(3):
       DUDT = getdUdTHeat(P);
       break;
    case(4):
       DUDT = getdUdtTransport(P);
       break;
    }

}







void DECISION::getNextDecisionEuler(int num_method) {
    DECISION P(1);
    double **DUDT = new double*[CNT_DX];
    for(int i = 0; i < CNT_DX; i++) {
        DUDT[i] = new double[CNT_BAS];
    }
    copy_prev();
    getP(P);
    getDUDT(num_method, P, DUDT);
    for(int i = 0; i < CNT_DX; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            y[i][j] = y_prev[i][j] +  DUDT[i][j] * DT;
        }
    }
    for(int i = 0; i < CNT_DX; i++) {
        delete[] DUDT[i];
    }
    delete[] DUDT;
}





void DECISION::getNextDecisionRK3(int num_method) {

   
    DECISION P(1);
    double** y_2;
    double** y_3;
    double** y_4;
    copy_prev();  // y_prev
            /*   y_prev = y[t_CNT_DX]
    for(                           
    */

   
    #pragma omp parallel for
    for(int i = 0; i < CNT_DX; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            P.y_prev[i][j] = y_prev[i][j];
        }
    }  

    getP(P);

    getDUDT(num_method, P, y_2);
     
        
    #pragma omp parallel for
    for(int i = 0; i < CNT_DX; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            y[i][j] = y_prev[i][j]  + y_2[i][j] * DT; //Y(i+0.5)
        }
    }
  


    getP(P); //  p
    getDUDT(num_method, P, y_3);

    
    #pragma omp parallel for 
    for(int i = 0; i < CNT_DX; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            y[i][j] = 3.0 / 4.0 * y_prev[i][j] + 1 / 4.0 * y[i][j] + 0.25 * DT * y_3[i][j];
       }
    }

    
    getP(P);
    getDUDT(num_method, P, y_4);

    #pragma omp parallel for
    for(int i = 0; i < CNT_DX; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            y[i][j] = 1 / 3.0 * y_prev[i][j] + 2.0 / 3.0 * y[i][j] + 2.0 / 3.0 * DT * y_4[i][j];
       }
    }

    #pragma omp parallel for
    for(int i = 0; i < CNT_DX; i++) {
        delete[] y_2[i];
        delete[] y_3[i];
        delete[] y_4[i];
    }

    //P.~DECISION();  
    delete[] y_2;
    delete[] y_3;
    delete[] y_4;


}





