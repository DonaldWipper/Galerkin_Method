#ifndef DECISION_H
#define DECISION_H




#include <fstream>
#include <iostream>
#include <time.h>
#include <vector>


#include "Consts.h"
#include "matrix.h"





/**********************************************
 *        Class of decision                   * 
 *    includes:                               *  
 * 1) MATRIX CNT_DX X CNT_BAS. Where 
 *     CNT_DX -  , CNT_BAS - order of approximate polynom 
   2) 
***********************************************/

class DECISION {
public:
    int type; // Flag defining type of decision
    /* ******************
       * 0 - decision U *
       * 1 - decision P *
       ******************/


   double **y;
   double **y_prev;
  

   MATRIX *M;
 
   DECISION(int t) {
       
       M = new MATRIX();

               
       y = new double*[CNT_DX];
       y_prev = new double*[CNT_DX];
       
     

       for(int i = 0; i < CNT_DX; i++) {
           y[i] = new double[CNT_BAS];
           y_prev[i] = new double[CNT_BAS];
         
       }

       type = t;
        
   }



   int setDec(int t) {
       //M->~MATRIX();
       M = new MATRIX();

               
       y = new double*[CNT_DX];
       y_prev = new double*[CNT_DX];
    
     

       for(int i = 0; i < CNT_DX; i++) {
           y[i] = new double[CNT_BAS];
           y_prev[i] = new double[CNT_BAS];
       
       }

       type = t;
   }

   int remove() {
        delete M;
        for(int i = 0; i < CNT_DX; i++) {
        
           delete[] y[i];
           delete[] y_prev[i];
    
       }
       
       
       delete[] y;
       delete[] y_prev;
  


   }  

   ~DECISION() {
        
        delete  M;
       for(int i = 0; i < CNT_DX; i++) {
           delete[] y[i];
           delete[] y_prev[i];

       }
       
       delete[] y;
       delete[] y_prev;
 
   }


   void initialize();   //���������������� ��������� �������
   double getDecInnary(int k, double x); //����������� ������� �� ��������
   double getDecInnary2(int k, double x); 
    
   int updateDec(int type); 
   
   double getDecBounds(int k, double x, int pType = -1);
   /*   mType - type of flux
        -1 - take from TYPE_FLUX        
        0 - U+, P-
        1 - U-, P+
        2 - (U+ + U-) / 2  and (P+ + P-) / 2
        3 - (U+ + U-) / 2 + (U_prev+ - U_prev-) and (P+ + P-_) / 2 
        4 - (U+ + U-) / 2  and (P+ + P-_) / 2 + CK(U+ - U-)
        5 - (U+ + U-) / 2  and (P+ + P-_) / 2 + [(P+ + P-) / (U+ + U-)] * (U+ - U-)  Custom constant 

   */   
   double getFullDec(double x);
   
   //file for saving
   
   /*
      num_step - number of R-K step  
   */

   void save_file(int num_step);     
   

   //file for recovering 
   void recover_dec();

   void copy(double **r);  //���������� ������-��������� � ������ �������
   void copy_prev();  //C�������� �������

   void export2gnuplot(const char * file_name, double t); 
   void getP(DECISION &P);
   double **getdUdTViscos(DECISION &P); //C ������� ������� p ������� dUdt
   //double **getdUdTBurgers1(DECISION &P);
   double **getdUdTBurgers2(DECISION &P);
   double **getdUdTHeat(DECISION &P);
   double **getdUdtTransport(DECISION &P);

   void getDUDT(int num_method, DECISION &P, double** &DUDT); //���������� ����������� � ����������� �� ���� ������

   /*
    1 - Transport
    2 - Transport with Viscosity
    3 - Heat
    4 - Burgers 1
    5 - Burgers 2
    */


   
   int getType();
   void getNextDecisionRK3(int num_method);
   void getNextDecisionEuler(int num_method);

   //new
   
   double getPowY(int i, int k, double x);
   double getIntegral(double a, double b, int ii, int CNT_BAS); 
  
};











#endif // DECISION_H
