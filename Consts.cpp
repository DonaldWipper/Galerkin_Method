#include "Consts.h"

//Transport with viscosity
/*extern const double A = -M_PI;
extern const double B = M_PI;*/

//For heat equition

/*


extern const double A = 0.0;
extern const double B = 2 *  M_PI;
*/




extern const double A = - M_PI;
extern const double B = M_PI;


/*

extern const double A = -1.0;
extern const double B = 1.0;
*/

extern const double EPS_HEAT = 1e-16; // Accuracy of precise heat equation


extern int CNT_DX = 10;      //A number of space's step  
extern const int M =1;
extern const int CNT_PNT_SEG = 1; //A count points of segment
extern int CNT_BAS = 2;  // A number of basis functions


extern double NU = 0.2; //viscocity for burgers


/* 1 - CONST 
   2 - LINEAR
   3 - QUADRIC
   4 - CUBIC  
   5 - 
*/

extern int MAX_CNT_DX = 3;  // A number of  max calculated space step iterations
extern int MAX_CNT_BAS = 5; // A number of  max calculated order of basis functions  




   
extern double DT = 1e-4;
extern double DX = (B - A) / CNT_DX;
extern int COUNT_ITER =   0.7 * 1e4;
extern double CK = 1;    //Constant for flux with type #4, #5, #6, #7
 
extern double UK = 100;

extern int TYPE_FLUX = 1;

enum FLUXES  {UPLUS_PMINUS, UMINUS_PPLUS, HALF_SUMM, HALF_AND_HALF_CONST, CUSTOM};
/*    0 - U+, P-
      1 - U-, P+
      2 - (U+ + U-) / 2  and (P+ + P-) / 2
      3 - (U+ + U-) / 2  and (P+ + P-_) / 2 + CK(U+ - U-)
      4 - (U+ + U-) / 2  and (P+ + P-_) / 2 + [(P+ + P-) / (U+ + U-)] * (U+ - U-)  Custom constant 
      5 - (U+ + U-) / 2  and (P+ + P-)  / 2 + / 
      6 -      

*/ 




extern const int NUM_METH = 0;
extern double T = 2;

/* 0 - Transport with viscosity
   1 - Burgers 1
   2 - Burgers 2
   3 - Heat
   4 - Transport

*/










