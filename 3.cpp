#include <iostream>
#include "functions.h"
#include "decision.h"
#include <iostream>
#include <sstream>
#include <string>
#include <omp.h>
#include "Consts.h"

std::string getStrNameMethod(int n) {
    switch (n) {
    case(0):
        return std::string("Transport with viscosity");
        break;
    case(1):
        return std::string("Burgers 1");
        break;
    case(2):
        return std::string("Burgers 2");
        break;
    case(3):
        return std::string("Heat");
        break;
    case(4):
        return std::string("Transport");
        break;
    default:
        break;
    }
}


std::string getStrFlux(int n) {
    std::string res;
    std::string lConstStr;
    std::ostringstream strs;
    strs << CK;
    lConstStr = strs.str();
     
    switch (n) {
    case(0):
        return std::string("U+, P-");
        break;
    case(1):
        return std::string("U-, P+");
        break;
    case(2):
        return std::string("0.5 * (U- + U+)");
        break;
    case(3):
        return std::string("diff");
        break;
    case(4):
        res = "(U+ + U-) / 2  and (P+ + P-_) / 2";
        res += "+";
        res += lConstStr;
        res += "(U+ - U-)";
        return res;
        break;
    case(5):
        return std::string("(U+ + U-) / 2  and (P+ + P-_) / 2 + [(P+ + P-) / (U+ + U-)] * (U+ - U-)");  
        break;
    default:
        break;
    }
}





void getRates(int k, double tt, DECISION* Y, double &L1, double &L2) {
    double a = A + k * DX;
    double b = A + (k + 1) * DX;
    double integralL1 = 0, integralL2 = 0;
    double step = (b - a) / 10.0;
    double t[5];
    double c[5];
    t[0] = 0.906179845938664;
    t[1] = 0.538469310105683;
    t[2] = 0.0;
    t[3] = -t[1];
    t[4] = -t[0];
    c[0] = 0.236926885056189;
    c[1] = 0.478628670499366;
    c[2] = 0.568888888888889;
    c[3] = c[1];
    c[4] = c[0];
    for(int i = 0; i < 10; i++) {   
        double x1 = a + i * step;
        double x2 = a + (i + 1) * step;
        double integralL11 = 0;
        double integralL22 = 0;

        for(int j = 0; j < 5; j++) {   //Calculate integral with use of quadratic formules
            double x = (x2 - x1) * (t[j] + 1) / 2.0 + x1;
            double MM = fabs(getPreciseDec(x, tt) - Y->getDecInnary(k, x));
            integralL11 += c[j] * MM;
            integralL22 += c[j] * pow(MM, 2);
        }
        integralL1 += integralL11 * (x2 - x1) / 2;
        integralL2 += integralL22 * (x2 - x1) / 2;

    }
    L1 = integralL1;
    L2 = integralL2;
}













int main(int argc, char *argv[])
{
    FF(0.5, 0.5);
    FF(0.3, 0.5);
    FF(0.1, 0.1);
    FF(0.5, 0.1);
    FF(0.5, 0.7);
    FF(0.7, 0.7);  
    FF(0.5, 1.0);
    FF(0.7, 1.0);  
 


    return 1;
}
