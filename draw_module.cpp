#include ""



DRAW_SCENE::DRAW_SCENE(QWidget* pwgt) : QWidget(pwgt) {
    view = new QGraphicsView();
    scene = new QGraphicsScene(QRectF(0, 0, 10, 10));
    view->setScene(scene);
    pen = new QPen(Qt::red);
    pen->setWidth(1);
    Y = new DECISION(N, A, B, 0);

    QLabel* plblDT = new QLabel("DT");
    QLabel* plblN = new QLabel("N");

    QSplitter *splitter = new QSplitter(Qt::Horizontal);
    QWidget *wgt1 = new QWidget();
    view->setMinimumSize(100, 100);

    SCALE_X = 200;
    SCALE_Y = 200;

    lnDT = new QLineEdit();
    lnN = new QLineEdit();
    lnDT->setText(QString::number(DT));
    lnN->setText(QString::number(N));
    QHBoxLayout* phbxLayout = new QHBoxLayout;
    QGridLayout* ptopLayout = new QGridLayout;
    ptopLayout->addWidget(lnDT, 0, 1);
    ptopLayout->addWidget(lnN, 1, 1);
    ptopLayout->addWidget(plblDT, 0, 0);
    ptopLayout->addWidget(plblN, 1, 0);
    wgt1->setLayout(ptopLayout);
    splitter->addWidget(wgt1);
    splitter->addWidget(view);
    phbxLayout->addWidget(splitter);
    setLayout(phbxLayout);
}
