#include "ko_vector.h"


KO_VECTOR::KO_VECTOR() {
    mSize = 0; 
}

KO_VECTOR::~KO_VECTOR() {
    //delete[] mPtr;
}
 
KO_VECTOR::KO_VECTOR(int pSize) {
    mSize = pSize;
    mPtr = new double[pSize];  
    for(int i = 0; i < mSize; i++) {
        mPtr[i] = 0.0;
    }
}

KO_VECTOR::KO_VECTOR(double *pVector, int pSize) {
    mSize = pSize;
    mPtr = pVector; 
}

double& KO_VECTOR::operator[](int i) {
    return mPtr[i]; 
}
 
KO_VECTOR KO_VECTOR::operator*(double** matrix) {
    KO_VECTOR v(mSize);
    for(int i = 0; i < mSize; i++) {
        for(int j = 0; j < mSize; j++) {
            v[i] += mPtr[j] * matrix[i][j];
        }
    }
    return v;       
}    

KO_VECTOR KO_VECTOR::operator/(float c) {
    KO_VECTOR v(mSize);
    for(int i = 0; i < mSize; i++) {
        v[i] = mPtr[i] / c;
    }
    return v;       
}

KO_VECTOR KO_VECTOR::operator+(const KO_VECTOR &v2) {
    KO_VECTOR v(mSize);
    for(int i = 0; i < mSize; i++) {
        v[i] = mPtr[i] + v2.mPtr[i];
    }
    return v;       
}


KO_VECTOR KO_VECTOR::operator-(const KO_VECTOR &v2) {
    KO_VECTOR v(mSize);
    for(int i = 0; i < mSize; i++) {
        v[i] = mPtr[i] - v2.mPtr[i];
    }
    return v;       
}
   
KO_VECTOR& KO_VECTOR::operator=(KO_VECTOR& v) {
    for(int i = 0; i < mSize; i++) {
        mPtr[i] = v.mPtr[i];
    }
    return *this;
}

KO_VECTOR KO_VECTOR::operator= (const KO_VECTOR &v) {
    for(int i = 0; i < mSize; i++) {
        mPtr[i] = v.mPtr[i];
    }
}
