#include "matrix.h"


int MATRIX::getReverseMatrix(int pN,double **pMatrInit, double **pRevMatrix)
{
   double **pMatrix;
   pMatrix = new double*[pN];
   for(int i = 0; i < pN; i++) {
       pMatrix[i] = new double[pN]; 
   }
   //Make intitial matrix
   for(int i = 0; i < pN; i++) {
       for(int j = 0; j < pN; j++) {
           pMatrix[i][j] = pMatrInit[i][j];
           pRevMatrix[i][j] = 0;
           pRevMatrix[i][i] = 1;
       }
   }
   double a, b;
   for(int i = 0; i < pN; i++) {
       a = pMatrix[i][i];   
       for(int j = i + 1; j < pN; j++) {
           b = pMatrix[j][i];
           for(int k = 0; k < pN; k++) {
               pMatrix[j][k] = pMatrix[i][k] * b - pMatrix[j][k] * a;
               pRevMatrix[j][k] = pRevMatrix[i][k] * b - pRevMatrix[j][k] * a;
           }
        }
   } 
   //
   double lSum;
   for(int i = 0; i < pN; i++) {
       for(int j = pN - 1; j >= 0; j--) {
           lSum = 0; 
           for(int k = pN - 1; k > j; k--) {
               lSum += pMatrix[j][k] * pRevMatrix[k][i];
           } 
           if(pMatrix[j][j] == 0) {
               
               for(int i = 0; i < pN; i++) {
                   delete[] pMatrix[i];
               }
               delete[] pMatrix; 
               return 0;
           }
           pRevMatrix[j][i] = (pRevMatrix[j][i] - lSum) / pMatrix[j][j];
       }
   }
   for(int i = 0; i < pN; i++) {
       delete[] pMatrix[i];
   }
   delete[] pMatrix; 
   return 1;
} 



int MATRIX::getMatrixFi(double **matrix) {
    for(int i = 0; i < CNT_BAS; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            matrix[i][j] = integralFi(i, j) / DX;
        }
    }
    return 0;
}


int MATRIX::getDerMatrix(double **matrix) {
    for(int i = 0; i < CNT_BAS; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            matrix[i][j] = integralDerFi(i, j);
        }
    }
    return 0;
}


int MATRIX::get3DerMatrix(double ***matrix) {
    for(int i = 0; i < CNT_BAS; i++) {
        for(int j = 0; j <  CNT_BAS; j++) {
            for(int k = 0; k < CNT_BAS; k++) {
                matrix[i][j][k] = integral3DerFi(i, j, k);
            }
        }
    }
    return 0; 
} 

MATRIX::MATRIX() {
    
    mMatrixFi = new double*[CNT_BAS];
    mMatrixDer = new double*[CNT_BAS];
    mMatrixRev = new double*[CNT_BAS];
    mMatrix3Der = new double**[CNT_BAS];


    for(int i = 0; i < CNT_BAS; i++) {
        mMatrix3Der[i] = new double*[CNT_BAS];
    }
     
    for(int i = 0; i < CNT_BAS; i++) {
        for(int j = 0; j < CNT_BAS; j++) {
            mMatrix3Der[i][j] = new double[CNT_BAS];
        } 
    } 
       
    
    for(int i = 0; i < CNT_BAS; i++) {
        mMatrixFi[i] = new double[CNT_BAS];
        mMatrixDer[i] = new double[CNT_BAS];
        mMatrixRev[i] = new double[CNT_BAS];
    } 


    getMatrixFi(mMatrixFi);
    getReverseMatrix(CNT_BAS, mMatrixFi, mMatrixRev);
    getDerMatrix(mMatrixDer);
    get3DerMatrix(mMatrix3Der);

}


MATRIX::~MATRIX() {
  
    for(int i = 0; i < CNT_BAS; i++) {
      for(int j = 0; j < CNT_BAS; j++) {
           delete[] mMatrix3Der[i][j];
       } 
   }
    

    for(int i = 0; i < CNT_BAS; i++) {
        delete [] mMatrixFi[i];
        delete [] mMatrixDer[i];
        delete [] mMatrixRev[i];
        delete [] mMatrix3Der[i];
    } 
 
 

    delete [] mMatrixFi;
    delete [] mMatrixDer;
    delete [] mMatrixRev;
    delete [] mMatrix3Der;
     
}



double MATRIX::MATRIX_INIT(int i, int j) {
    return mMatrixFi[i][j];
}

double MATRIX::MATRIX_DER(int i, int j) {
   return mMatrixDer[i][j];

}

double MATRIX::MATRIX_3DER(int i, int j, int k) {
   return mMatrix3Der[i][j][k];
}



double MATRIX::MATRIX_REV(int i, int j) {
    return mMatrixRev[i][j];

}

