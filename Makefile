# Это комментарий, который говорит, что переменная CC указывает компилятор, используемый для сборки
CC=g++
#Это еще один комментарий. Он поясняет, что в переменной CFLAGS лежат флаги, которые передаются компилятору
CFLAGS=-c -Wall -g -fopenmp
LDFLAGS = -fopenmp

all: main

main: decision.o Consts.o main.o functions.o burgers_solution.o matrix.o
	$(CC) $(LDFLAGS) main.o decision.o Consts.o functions.o  matrix.o burgers_solution.o  -o main


matrix.o: matrix.cpp
	$(CC) $(CFLAGS) matrix.cpp



Consts.o: Consts.cpp
	$(CC) $(CFLAGS) Consts.cpp

functions.o: functions.cpp
	$(CC) $(CFLAGS) functions.cpp 

decision.o: decision.cpp
	$(CC) $(CFLAGS) decision.cpp

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp


burgers_solution.o: burgers_solution.cpp
	$(CC) $(CFLAGS) burgers_solution.cpp

clean:
	rm -rf *.o main
