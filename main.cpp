#include <iostream>
#include "functions.h"
#include "decision.h"
#include <iostream>
#include <sstream>
#include <string>
#include <omp.h>
#include <stdlib.h>
#include <time.h>
#include "Consts.h"

std::string getStrNameMethod(int CNT_DX) {
    switch (CNT_DX) {
    case(0):
        return std::string("Convection diffusion");
        break;
    case(1):
        return std::string("Burgers 1");
        break;
    case(2):
        return std::string("Burgers ");
        break;
    case(3):
        return std::string("Heat");
        break;
    case(4):
        return std::string("Transport");
        break;
    default:
        break;
    }
}


std::string getStrFlux(int CNT_DX) {
    std::string res;
    std::string lConstStr;
    std::ostringstream strs;
    strs << CK;
    lConstStr = strs.str();
     
    switch (CNT_DX) {
    case(0):
        return std::string("$ {p^ - },{u^ + } $");
        break;
    case(1):
        return std::string("$ {p^ + },{u^ - } $");
        break;
    case(2):
        return std::string("$ \\frac{{{p^ + } + {p^ - }}}{2},\\frac{{{u^ + } + {u^ - }}}{2} $");
        break;
    case(3):
        res = "$ \\frac{{{p^ + } + {p^ - }}}{2}";
        res += "+";
        res += lConstStr;
        res += "({u^ + } - {u^ - }),\\frac{{{u^ + } + {u^ - }}}{2} $";
        return res;
        break;
    case(4):
        //return std::string("(U+ + U-) / 2  and (P+ + P-_) / 2 + [(P+ + P-) / (U+ + U-)] * (U+ - U-)");  
        res =  "$  \\frac{{{p^ + } + {p^ - }}}{2} +";
        res += lConstStr;
        res += "|\\frac{ {{p^ + } + {p^ - }}}{{|{u^ + }| + | {u^ - }| }}| * ({{u^ + } - {u^ - } }),\\frac{{{u^ + } + {u^ - }}}{2}    $"; 
        return res; 
    case(5):
        //return std::string("(U+ + U-) / 2  and (P+ + P-_) / 2 + [(P+ + P-) / (U+ + U-)] * (U+ - U-)");  
        res =  "$  \\frac{{{p^ + } + {p^ - }}}{2} +";
        res += lConstStr;
        res += " \\frac{{\left| {{p^ + }} \\right| + \\left| {{p^ - }} \\right|}}{{|{u^ + }| + |{u^ - }|}}|*({u^ + } - {u^ - } + {DX} * {{p^ + } + {p^ - }} ),\\frac{{{u^ + } + {u^ - }}}{2}  $"; 
        return res;  
        break;
     case(6):
        //return std::string("(U+ + U-) / 2  and (P+ + P-_) / 2 + [(P+ + P-) / (U+ + U-)] * (U+ - U-)");  
        res = "$  \\frac{{{p^ + } + {p^ - }}}{2} +";
        res += lConstStr;
        res += "\\frac{{\\left| {{p^ + } + {p^ - }} \\right|}}{{|{u^ + }| + |{u^ - }|}}|*({u^ + } - {u^ - } + {DX} * {{p^ + } + {p^ - }} ),\\frac{{{u^ + } + {u^ - }}}{2} $"; 
        return res; 
        break;
     case(7):
        //return std::string("(U+ + U-) / 2  and (P+ + P-_) / 2 + [(P+ + P-) / (U+ + U-)] * (U+ - U-)");  
        res = "$  \\frac{{{p^ + } + {p^ - }}}{2} +";
        res += lConstStr;
        res += "|\\frac{ {{p^ + } + {p^ - }}}{{|{u^ + }| + | {u^ - }| }}| * ({{u^ + } - {u^ - } + {DX} * {{p^ + } + {p^ - }}  }),\\frac{{{u^ + } + {u^ - }}}{2}    $"; 
        break;
    default:
        break;
    }
}





void getRates(int k, double tt, DECISION* Y, double &L1, double &L2) {
    double a = A + k * DX;
    double b = A + (k + 1) * DX;
    double integralL1 = 0, integralL2 = 0;
    double cnt_steps = 5.0;
    double step = (b - a) / cnt_steps;
    double t[5];
    double c[5];
    t[0] = 0.906179845938664;
    t[1] = 0.538469310105683;
    t[2] = 0.0;
    t[3] = -t[1];
    t[4] = -t[0];
    c[0] = 0.236926885056189;
    c[1] = 0.478628670499366;
    c[2] = 0.568888888888889;
    c[3] = c[1];
    c[4] = c[0];
    for(int i = 0; i < cnt_steps; i++) {   
        double x1 = a + i * step;
        double x2 = a + (i + 1) * step;
        double integralL11 = 0;
        double integralL22 = 0;

        for(int j = 0; j < 5; j++) {   //Calculate integral with use of quadratic formules
            double x = (x2 - x1) * (t[j] + 1) / 2.0 + x1;
            double MM = fabs(getPreciseDec(x, tt) - Y->getDecInnary(k, x));
            integralL11 += c[j] * MM;
            integralL22 += c[j] * pow(MM, 2);
        }
        integralL1 += integralL11 * (x2 - x1) / 2;
        integralL2 += integralL22 * (x2 - x1) / 2;

    }
    L1 = integralL1;
    L2 = integralL2;
}





std::string timeformat(double ss){
        std::ostringstream strs; 
        int sec = floor(ss);
        int minutes = sec / 60 % 60;
        int hours = sec / 3600  % 24;
        int s = sec % 60;
        strs << hours << "h:" << minutes << "m:" << s << "s";
        return strs.str();    
}






int main(int argc, char *argv[])
{
    
    
    double L1[5];
    double L2[5];
    double start = omp_get_wtime();
    double CKK[6];
    CKK[0] = 0.3;
    CKK[1] = 0.5;
    
    //CKK[1] = 0.5;
    CKK[2] = 0.8;
    //CKK[3] = 1.0;
    //CKK[4] = 2.0;
    CKK[3] = 40.0;
    CKK[4] = 60.0;
    CKK[5] = 80.0;

    double UKK[2];
    UKK[0] = 0.5;
    UKK[1] = 100.0; 
    

    //Open all files for saving 
    std::ofstream outL1("L1NEW.txt", std::ios::app);        
    std::ofstream outL2("L2NEW.txt", std::ios::app); 
    std::ofstream outOrderL1("LO1NEW.txt", std::ios::app);        
    std::ofstream outOrderL2("LO2NEW.txt", std::ios::app); 
    std::ofstream outLog("Log.txt", std::ios::app); 
  
    std::ifstream outBackUp("backup.txt");   
    long int ttime = time (NULL);

    // С помощью функции ctime преобразуем считанное время в
    // локальное, а затем в строку и выводим в консоль.
    outLog << "Время: " << ctime (&ttime)  << std::endl;

    //TYPE_FLUX = 5;
    int START_CNT_DX = CNT_DX;
   
    DECISION *Y;
    Y = new DECISION(0);
    Y->initialize();
    //Y->updateDec(0);

    int CNT_BAS_ST  = CNT_BAS; //initialize first basis func   
    
    for(int k = 9; k < 10; k ++) { // Iteration for fluxes
       
       if((k >= 3) && (k <= 8)) {
           TYPE_FLUX = 3; 
           CK = CKK[k - 3];   
       } else {
           TYPE_FLUX = k;
           if(k == 9) {
               TYPE_FLUX = 4; 
           }
           if(k == 10) {
               TYPE_FLUX = 5; 
           }
           if(k == 11) {
               TYPE_FLUX = 6; 
           }
           if(k == 12) {
               TYPE_FLUX = 7; 
           }

       }  
    
       outL1 <<  getStrFlux(TYPE_FLUX) << " & ";
       outL2 <<  getStrFlux(TYPE_FLUX) << " & ";
       outOrderL2 <<  getStrFlux(TYPE_FLUX) << " & ";
       outOrderL1 <<  getStrFlux(TYPE_FLUX) << " & ";
       std::cout << "Flux #  " << TYPE_FLUX << std::endl; 

     
          

       for(int j = CNT_BAS_ST; j <= MAX_CNT_BAS; j++) { // A number of basis functions
       
           ttime = time (NULL);
           outLog <<  ctime(&ttime) << "Counting basis function # " << j << std::endl;
      
            if(j != CNT_BAS_ST) { //
               outL1 << " & ";
               outL2 << " & ";
               outOrderL2 <<  " & ";
               outOrderL1 <<  " & "; 
           }


           CNT_BAS = j;
           outL1 << j - 1 << " & ";
           outL2 << j - 1 << " & ";
           outOrderL2 << j - 1 << " & ";
           outOrderL1 << j - 1 << " & ";
                               
           std::cout << "A number of basis functions is " << CNT_BAS << std::endl;
           CNT_DX = START_CNT_DX;
           DX = (B - A) / CNT_DX;
           
           
           for(int i = 0; i <= MAX_CNT_DX; i++) {
             
               //DECISION *Y;
               //Y = new DECISION(0);
               //Y->initialize();
               Y->setDec(0);
               Y->initialize();
          
             
              
               ttime = time (NULL);
           
               outLog <<  ctime(&ttime) << " Count decision for space steps " << CNT_DX << std::endl; 
               
               double t = 0;
               T = t;
               std::cout << "Start calculate method " << getStrNameMethod(NUM_METH) << std::endl;
               std::cout << "DX = " << DX << "DT = " << DT << ", " << "COUNT ITERATIONS = " << COUNT_ITER << std::endl;

               //open file for graphs
               
              

               for(int num_step = 0; num_step < COUNT_ITER + 1; num_step++) {
                   std::cout << "Method " << getStrNameMethod(NUM_METH) << std::endl;
                   std::cout << "DX = " << DX << ", DT = " << DT << ", t= " << t << std::endl;
                   std::cout << "Step # " << num_step << " calculated " << ", left " <<  COUNT_ITER - num_step << std::endl;
                   ttime = time (NULL);
                   


                       
                   int J =  COUNT_ITER / 100;  
                   std::cout << J << std::endl;
                   if(num_step % J == 0) {
                      
                       Y->export2gnuplot("data100.dat", t);
                       std::ofstream outScript("scriptplot.gp", std::ios::trunc); //Output script for graph  
                        outScript << "#!/usr/bin/gnuplot" << std::endl << std::endl;
                       outScript << "set terminal png " << std::endl;
                       outScript << "set output 'Galerkin" << num_step / J  << ".png' " << std::endl;
                       //outScript << "set yrange [-3:3]"<< std::endl;
                       outScript << "set yrange [-1.0:1.0]"<< std::endl;
                       outScript << "set ytics 0.1" << std::endl;
                       outScript << "set title '" << getStrNameMethod(NUM_METH) << " equation, DT = " << DT 
                                 << ", K = " << CNT_BAS - 1
                                 << ", " << "N = " << CNT_DX 
                                 << ", " << "DX = " << DX
                                 << ", T = " <<                     t  << "'" << std::endl;  
                      outScript << "plot 'data100.dat' using 1:2 title 'Approximate dec' with lines,  'data100.dat' using 1:3 title 'Precise dec' with lines" <<  std::endl; 
                       system("gnuplot scriptplot.gp");
                      outScript.close();
                   }


  
                   if(num_step == COUNT_ITER)  {

                       double L1Rate = 0, L2Rate = 0;
                       double max = -1000;
                       for(int i = 0; i < CNT_DX; i++) {
                           std::cout << i << " " << "calculate rates..." << std::endl;
                           double L1, L2;  
                          
                           getRates(i, t, Y, L1, L2); 
                           L1Rate += L1;
                           L2Rate += L2;
                       }
                       L1[i] = L1Rate;
                       L2Rate = sqrt(L2Rate);
                       L2[i] = L2Rate;
                       
                       ttime = time (NULL);
                       outLog <<  ctime(&ttime) << " L1 = " << L1[i] << "; " << "L2 = " << L2[i] <<  std::endl;

                       if(i != MAX_CNT_DX) {    
                           outL1 <<  L1[i] << " & ";
                           outL2 <<  L2[i] << " & ";
                       } else {
                           outL1 <<  L1[i] << " \\" << "\\" << std::endl;
                           outL2 <<  L2[i] << " \\" << "\\" << std::endl;
                           outOrderL1 <<  log(L1[i - 1] / L1[i]) / log(2)  << " \\" << "\\" << std::endl;
                           outOrderL2 <<  log(L2[i - 1] / L2[i]) / log(2)  << " \\" << "\\" << std::endl;
                       }
                       if((i > 0) && (i != MAX_CNT_DX)) {
                           outOrderL1 <<  log(L1[i - 1] / L1[i]) / log(2)  << " & ";
                           outOrderL2 <<  log(L2[i - 1] / L2[i]) / log(2)  << " & ";
                       }
                   }

                   if(num_step != COUNT_ITER) {
                       Y->getNextDecisionRK3(NUM_METH);
                       t += DT;
                       T = t;
                   }
                   Y->save_file(num_step);
                    
                  
               }
               std::cout << "label 2 " << std::endl;
               
               if(i == 0) {
                /*
                outScript << "#!/usr/bin/gnuplot" << std::endl << std::endl;
                outScript << "set terminal png " << std::endl;
                outScript << "set output 'Galerkin.png' " << std::endl;
                outScript << "set title '" << getStrNameMethod(NUM_METH) << ", DT = " << DT << ", " << "DX = " << DX << ", T = " << t << ", K = " << CNT_BAS << "'" << std::endl;
    
    outScript << "plot 'data100.dat' using 1:2 title 'Approximate dec' with lines,  'data100.dat' using 1:3 title 'Precise dec' with lines" <<  std::endl; 
                outScript.close();
                 */
                std::ofstream outGraph("data100.dat", std::ios::trunc);    //Output graph

               double h = DX / M;
               for(int l = 0; l <= CNT_DX; l++) {
                   for(int j = 0; j  < M; j++) {
                       if((l == CNT_DX) && (j > 0)) {
                           break;
                       }
                       double x = A + l * DX + j * h;
                       double U;
                       double UP = getPreciseDec(x, t);
                       if(j == 0) {
                           U = Y->getDecBounds(l, x);
                       } else {
                           U = Y->getDecInnary(l, x);
                       }
                       //std::cout << x << " " << U << " " << UP  << std::endl;
                       outGraph << x << " " << U << " " << UP  << std::endl;
               
                   }
               }
                outGraph.close();
                Y->remove();
              
           }
           
           CNT_DX *= 2;
           DX = (B - A) / CNT_DX;
          
           //std::cout << "Y " << A << " " << Y->getDecBounds(0, A);   
           }
          
       }
       outL1 <<  "\\hline" << std::endl;
       outL2 <<  "\\hline" << std::endl;
       outOrderL1 <<  "\\hline" << std::endl;
       outOrderL2 <<  "\\hline" << std::endl;
    }  
    //Program is finished with sucess
    std::ofstream outBackUp2("backup.txt");  
    outBackUp2 << 1 << std::endl;
    
    
      
   
    std::cout << "Time of executing program " << timeformat(omp_get_wtime() - start) << std::endl;
    outL1.close();
    outL2.close(); 
    outOrderL1.close();
    outOrderL2.close();  
  
    return 1;
}
