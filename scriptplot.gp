#!/usr/bin/gnuplot

set terminal png 
set output 'Galerkin100.png' 
set yrange [-1.0:1.0]
set ytics 0.1
set title 'Convection diffusion equation, DT = 0.0001, K = 4, N = 80, DX = 0.0785398, T = 0.7'
plot 'data100.dat' using 1:2 title 'Approximate dec' with lines,  'data100.dat' using 1:3 title 'Precise dec' with lines
