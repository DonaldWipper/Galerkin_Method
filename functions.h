#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <math.h>
#include <iostream>
#include "Consts.h"
#include "burgers_solution.h"


double getPreciseDec(double x2, double t);


//Initial function for t = 0
double phi(double x);
double phi2(double x);
double phi3(double x);


double getCentre(int k);

//Basis pow( (x - x_c) / DX, i)
double getFi(int i, int k, double x); //++++++++++++++++++++++
/* i - order of basis
   k - number of segment
   x - argument
*/


/* phi *  pow( (x - x_c) / DX, i) 
   xc  [A + k * DX, A + (k + 1) * DX] 
*/


double getF(int i, int k, double x);       //Get function for initial integral
double integral(int i, int k);             //Get integaral for each basis * phi in 1st segment



//integral Phi(i) * Phi(j)
double FiFi(int i, int j, int k, double x); //++++++++++++++++++++++++++++++++++++++++++++++

//integral Phi(i) * Phi(j) * dPhi(l)
double FiFjdFk(int i, int j, int l, int k, double x);
double integral3DerFi(int i, int j, int m);

double integralFi(int i, int j); 

//integral Phi(i) * Phi'(j)
double FiDerFi(int i, int j, int k, double x);
double integralDerFi(int i, int j); 





double getIntegral(double a, double b, double xc, double (*f)(double x, double xc));


double getIntegral(double a, double b, int i, int k, double (*f)(int i, int k, double x));

double getIntegral(double a, double b, int ii, int jj, int k, double (*f)(int i, int j, int k, double x));

// i / DX * Fi * Fj * Fm
double getIntegral(double a, double b, int ii, int jj, int mm, int k, double (*f)(int i, int j, int m, int k, double x));



double getIntegral(double a, double b, double (*f)(double x));


double getIntegral(double a, double b,
                   double (*f)(double x, double x2, double t),
                   double x, double t);


double integral1(int k);
double integral2(int k);
double integral3(int k);








double get_part(int n) ;


double getPreciseDec(double x2, double t);
double get_precise_decision(int count_n, double x, double t);
double getPreciseHeatDecision(double x, double t);


double phi4(double x);


double getVisDecision(double x, double t); 






double FF(double x, double t);
double FFF(double x, double t); 



double I(double x, double x2, double t); 
double I2(double x, double x2, double t);
double F(double x, double t);




















#endif // FUNCTIONS_H
