#ifndef MATRIX_H
#define MATRIX_H

#include "Consts.h"
#include "functions.h"


/* class for loading matrices
    

*/


class MATRIX {
    double **mMatrixFi;
    double **mMatrixDer;
    double **mMatrixRev;

    double **mMatrixDD;
    double **mMatrixCC;
    double ***mMatrix3Der;
       

    int getReverseMatrix(int pN,double **pMatrix, double **pRevMatrix); 
    int getMatrixFi(double **matrix);
    int getDerMatrix(double **matrix);
    int get3DerMatrix(double ***matrix); 

public:
    MATRIX();
    ~MATRIX();
    double MATRIX_INIT(int i, int j);
    double MATRIX_DER(int i, int j);
    double MATRIX_REV(int i, int j);
    double MATRIX_3DER(int i, int j, int k);
};

#endif // MATRIX_H
