


class KO_VECTOR {
    int mSize;
    double *mPtr;
public:
    KO_VECTOR();
    ~KO_VECTOR(); 
    KO_VECTOR(int pSize);
    KO_VECTOR(double *pVector, int pSize); 

    double& operator[](int i); 
    KO_VECTOR operator*(double** matrix); 
    KO_VECTOR operator/(float c);
    KO_VECTOR operator+(const KO_VECTOR &v);
    KO_VECTOR operator-(const KO_VECTOR &v);

    KO_VECTOR& operator=(KO_VECTOR& v);
    KO_VECTOR operator= (const KO_VECTOR &v);
};

