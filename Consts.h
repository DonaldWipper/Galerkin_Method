#ifndef CONSTS_H
#define CONSTS_H

#include <math.h>


extern const double A;

extern const double B;

extern const double EPS_HEAT;


extern  int CNT_DX;       //Number steps of x

extern const int M;  // 
extern const int CNT_PNT_SEG; 
extern int CNT_BAS;       // Number of basis functions

extern double DT;

extern double DX;

extern double T;

extern const int NUM_METH;
extern  int TYPE_FLUX;

extern double CK;
extern double UK;

extern double NU;
extern  int COUNT_ITER;
/* 0 - Transport with viscosity
   1 - Burgers 1
   2 - Burgers 2
   3 - Heat
   4 - Transport

*/



extern int MAX_CNT_DX;
extern int MAX_CNT_BAS;

/*double INTEG_1 = DX;
double INTEG_X2 = DX / 12;
double INTEG_X4 = DX / 80;
doubel INTEG_X6 = DX / 448
*/
extern double INTEG_X6;


/* ***********************************
   *  ������� ��������� FI_i * FI_j  *
   *********************************** */

extern const double D[3][3];

extern const double DD[3][6];



/* *************************************
   *  ������� ���������� FI_i * FI'_j  *
   *************************************  */

extern const double C[3][3];




extern const double CC[3][6];
/*double C[3][3] =  { {0, 1,  0},
                   {0, 0, 1 / 6.0},
                   {0, 1 / 12.0,  0} };*/


/* *********************************************
   *  �������� ������� ��������� FI_i * FI_j   *
   *********************************************  */

extern const double Areverse2[3][3];

extern const double Areverse1[2][2];


extern const double Areverse3[4][4]; 








#endif // CONSTS_H
