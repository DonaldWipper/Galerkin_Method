#include "functions.h"
#include "burgers_solution.h"
#include <omp.h>



/* Precise decision for each type of equation
   

*/


double getCentre(int k) {
    double x1 = A + k * DX;
    double x2 = A + (k + 1) * DX;
    double xc = (x1 + x2) / 2;  
    return  xc;
}

double getPreciseDec(double x2, double t) {
    double yp;
    switch(NUM_METH) {
    case(0):
        //yp = exp(-t / 4 + x2 / 2) * get_precise_decision(400, x2, t);
        yp = getVisDecision(x2, t);
        break;
    case(1):
        yp = FF(x2, t);
        break;
    case(2):
        yp = FF(x2, t);
        break;
    case(3):
        yp = getPreciseHeatDecision(x2, t);
        break;
    case(4):
        yp = sin(2 * M_PI * (x2 + t));
        break;
    }
    return yp;
}


//��������� �������
double phi(double x) {
    return sin(2 * M_PI * x) / exp(x / 2.0);
}

double phi2(double x) {
   //return sin(2 * M_PI * x); 
   return sin(x);
}   

double phi3(double x) {
    double sigma2 = 0.001;
    double nu = 1;
    //return 1 / (sqrt(sigma2) * sqrt(2 * M_PI)) * exp(-pow(x - nu, 2) / (2 * sigma2));
    //return 1 - cos(x);
    //return -sin(x) + 2;
    //return sin(x);
    return -sin(x);
}  


double phi4(double x) {
    return -sin(x) + 2; 
} 

double getVisDecision(double x, double t) {
    //return -exp(-M_PI * M_PI * t) * sin(M_PI * (x - t)); 
    return -exp(-t) * sin(x - t); 
}


//Basis function
double getFi(int i, int k, double x) {
    double xc = getCentre(k);   
    return pow((x - xc) / DX, i);  
} 

//Init function
double getF(int i, int k, double x) {
    return phi3(x) * getFi(i, k, x);
    //return phi3(x) * getFi(i, k, x);
    //return phi2(x) * getFi(i, k, x); 
}

double integral(int i, int k) {
   double a = A + k * DX;
   double b =  A + (k + 1) * DX;
   return getIntegral(a, b, i, k, getF);
}



double FiFi(int i, int j, int k, double x) {
    return getFi(i, k, x) * getFi(j, k, x);
}
 
double FiFjdFk(int i, int j, int l, int k, double x) {
    if(l == 0) {
        return 0;
    } else {
        return l / DX * getFi(i, k, x) * getFi(j, k, x) * getFi(l - 1, k, x);
    }
} 



double FiDerFi(int i, int j, int k, double x) {
    if(i == 0) {
        return 0;
    } else { 
        return i  / DX * getFi(i - 1, k, x) * getFi(j, k, x);
    }
}




double integralFi(int i, int j) {
   int k = 0;
   double a = A + k * DX;
   double b =  A + (k + 1) * DX;
   return getIntegral(a, b, i, j, k, FiFi);
}

double integralDerFi(int i, int j) {
   int k = 0;
   double a = A + k * DX;
   double b =  A + (k + 1) * DX;
   return getIntegral(a, b, i, j, k, FiDerFi);
}


double integral3DerFi(int i, int j, int m) {
   int k = 0;
   double a = A + k * DX;
   double b =  A + (k + 1) * DX;
   return getIntegral(a, b, i, j, m, k, FiFjdFk);
}





double getIntegral(double a, double b, double xc, double (*f)(double x, double xc)) {
    double integral = 0;
    double t[5];
    double c[5];
    t[0] = 0.906179845938664;
    t[1] = 0.538469310105683;
    t[2] = 0.0;
    t[3] = -t[1];
    t[4] = -t[0];
    c[0] = 0.236926885056189;
    c[1] = 0.478628670499366;
    c[2] = 0.568888888888889;
    c[3] = c[1];
    c[4] = c[0];
    double step = (b - a) / 100.0;
    for(int i = 0; i < 100; i++) {
        double x1 = a + i * step;
        double x2 = a + (i + 1) * step;
         double integral2 = 0;


        for(int j = 0; j < 5; j++) {
            double x = (x2 - x1) * (t[j] + 1) / 2.0 + x1;
            integral2 += c[j] * f(x, xc);
        }
        integral += integral2 * (x2 - x1) / 2;

    }


    return integral;
}


double getIntegral(double a, double b, int ii, int kk, double (*f)(int i, int k, double x)) {
    double integral = 0;
    double t[5];
    double c[5];
    t[0] = 0.906179845938664;
    t[1] = 0.538469310105683;
    t[2] = 0.0;
    t[3] = -t[1];
    t[4] = -t[0];
    c[0] = 0.236926885056189;
    c[1] = 0.478628670499366;
    c[2] = 0.568888888888889;
    c[3] = c[1];
    c[4] = c[0];
    double step = (b - a) / 100.0;
    
    for(int i = 0; i < 100; i++) {
        double x1 = a + i * step;
        double x2 = a + (i + 1) * step;
         double integral2 = 0;


        for(int j = 0; j < 5; j++) {
            double x = (x2 - x1) * (t[j] + 1) / 2.0 + x1;
            integral2 += c[j] * f(ii, kk, x);
        }
        integral += integral2 * (x2 - x1) / 2;

    }


    return integral;
}


double getIntegral(double a, double b, int ii, int jj, int mm, int k, double (*f)(int i, int j, int m, int k, double x)) {
    double integral = 0;
    double t[5];
    double c[5];
    t[0] = 0.906179845938664;
    t[1] = 0.538469310105683;
    t[2] = 0.0;
    t[3] = -t[1];
    t[4] = -t[0];
    c[0] = 0.236926885056189;
    c[1] = 0.478628670499366;
    c[2] = 0.568888888888889;
    c[3] = c[1];
    c[4] = c[0];
    double step = (b - a) / 100.0;
    for(int i = 0; i < 100; i++) {
        double x1 = a + i * step;
        double x2 = a + (i + 1) * step;
        double integral2 = 0;


        for(int j = 0; j < 5; j++) {
            double x = (x2 - x1) * (t[j] + 1) / 2.0 + x1;
            integral2 += c[j] * f(ii, jj, mm, k, x);
        }
        integral += integral2 * (x2 - x1) / 2;

    }


    return integral;
}


double getIntegral(double a, double b, int ii, int jj, int kk, double (*f)(int i, int j, int k, double x)) {
    double integral = 0;
    double t[5];
    double c[5];
    t[0] = 0.906179845938664;
    t[1] = 0.538469310105683;
    t[2] = 0.0;
    t[3] = -t[1];
    t[4] = -t[0];
    c[0] = 0.236926885056189;
    c[1] = 0.478628670499366;
    c[2] = 0.568888888888889;
    c[3] = c[1];
    c[4] = c[0];
    double step = (b - a) / 100.0;
    for(int i = 0; i < 100; i++) {
        double x1 = a + i * step;
        double x2 = a + (i + 1) * step;
        double integral2 = 0;


        for(int j = 0; j < 5; j++) {
            double x = (x2 - x1) * (t[j] + 1) / 2.0 + x1;
            integral2 += c[j] * f(ii, jj, kk, x);
        }
        integral += integral2 * (x2 - x1) / 2;

    }


    return integral;
}


double getIntegral(double a, double b, double (*f)(double x)) {
    double integral = 0;
    double t[5];
    double c[5];
    t[0] = 0.906179845938664;
    t[1] = 0.538469310105683;
    t[2] = 0.0;
    t[3] = -t[1];
    t[4] = -t[0];
    c[0] = 0.236926885056189;
    c[1] = 0.478628670499366;
    c[2] = 0.568888888888889;
    c[3] = c[1];
    c[4] = c[0];
    double step = (b - a) / 100.0;
    for(int i = 0; i < 100; i++) {
        double x1 = a + i * step;
        double x2 = a + (i + 1) * step;
        double integral2 = 0;


        for(int j = 0; j < 5; j++) {
            double x = (x2 - x1) * (t[j] + 1) / 2.0 + x1;
            integral2 += c[j] * f(x);
        }
        integral += integral2 * (x2 - x1) / 2;

    }

    return integral;
}


double getIntegral(double a, double b,
                   double (*f)(double x, double x2, double t),
                   double x, double t) {
    double integral = 0;
    double step = 0.01; 
    int N = floor ((b - a) / step);
    for(int i = 0; i < N; i++) {
        double x1 = a + i * step;
        double x2 = a + (i + 1) * step;
        double a = f(x, x1, t);
        double b = f(x, x2, t);
        integral += step * (a + b) / 2;
    }

    return integral;
}









double get_part(int n) {
    double l = B - A;
    double ksi = A;
    double integ = 0;

    double NN = 100;
    #pragma omp paraller for shared(integ)
    for(int i = 0; i < NN; i++) {
        ksi = A + (i + 0.5) * l / NN;
        integ += phi(ksi) * sin(M_PI * n * ksi / l) *  (l / NN);
    }
    return integ;
}

double get_part2(int n) {
    double l = B - A;
    double integral = 0;
    double step = (B - A) / 100.0;
    double t[5];
    double c[5];
    t[0] = 0.906179845938664;
    t[1] = 0.538469310105683;
    t[2] = 0.0;
    t[3] = -t[1];
    t[4] = -t[0];
    c[0] = 0.236926885056189;
    c[1] = 0.478628670499366;
    c[2] = 0.568888888888889;
    c[3] = c[1];
    c[4] = c[0];
    for(int i = 0; i < 100; i++) {
        double x1 = A + i * step;
        double x2 = A + (i + 1) * step;

        double integral2 = 0;
        for(int j = 0; j < 5; j++) {
            double x = (x2 - x1) * (t[j] + 1) / 2.0 + x1;
            integral2 += c[j] * phi2(x) * sin(M_PI * n * x / l);
        }
        integral += integral2 * (x2 - x1) / 2;

    }
    return integral;
    /*double NN = 100;
    for(int i = 0; i < NN; i++) {
        ksi = A + (i + 0.5) * l / NN;
        integ += phi2(ksi) * sin(M_PI * n * ksi / l) *  (l / NN);
    }
    return integ;*/
}






double get_precise_decision(int count_n, double x, double t) {
   double sum = 0;
   double l = B - A;
   #pragma omp paraller for
   for(int i = 1; i <= count_n; i++) {
      sum +=  (2.0 / l) * get_part(i) * sin(M_PI * i * x / l) * exp(-pow(M_PI * i / l, 2)  * t);
   }
   return sum;
}

double getPreciseHeatDecision(double x, double t) {
   double sum = 0, sum_prev = -1000;
   double l = B - A;
   int lMaxIter = 1000;
   int i = 0;
   while(true) {
      i++;
      if(i > lMaxIter) {
          break;
      }
      sum +=  (2.0 / l) * get_part2(i) * sin(M_PI * i * x / l) * exp(-pow(M_PI * i / l, 2)  * t);
      if(fabs(sum - sum_prev) < EPS_HEAT) {
          break;
      }  
      sum_prev = sum;
      //qDebug() << sum;
   }
   return sum;
}


//������ ��������
/*
   x, t - ���������� ������
   x2 - ������������� ����������
   ���������� �������
   exp[ - (1 / 2  * ( int{0, x2}(phi(x2)) + (x - x2) ^ 2 / (2 * t))) ] = F(x2)
*/




double I(double x, double x2, double t) {
    double A = getIntegral(0, x2, phi3); // int{0, x2}(phi(x2))
    double B = pow(x - x2, 2) / (2 * t);
    return exp(-1 / 2.0 * (A + B));
}



//�e����� ��������

/*  ���������� �������
   exp[ - (1 / 2  * ( int{0, x2}(phi(x2)) + (x - x2) ^ 2 / (2 * t))) ] * ( x - x2) / (2 * t) = F(x2)
*/

double I2(double x, double x2, double t) {
    double A = getIntegral(0, x2, phi3);
    double B = pow(x - x2, 2) / (2 * t);
    return exp(-1 / 2.0 * (A + B))  * (x - x2) / (1 * t);
}






double FF(double x, double t) {
    
    
    int qn = 20;
     double *qw;
     double *qx;
     double vu;
     double nu = NU;
     double c;
     double top = 0, bot = 0; 
     qx = new double[qn];
     qw = new double[qn];
     int qi;
     //return  burgers_solution2(nu, x, t);  
     hermite_ek_compute ( qn, qx, qw );

    for ( qi = 0; qi < qn; qi++ )
        {
          c = 2.0 * sqrt ( nu * t);

                  
          top = top + qw[qi]  * qx[qi] * 4 * nu 
            * exp ( -getIntegral(0, x - c * qx[qi], phi3)  
            / ( 2.0 * nu  ) );


          bot = bot + qw[qi] * c 
            *  exp ( -getIntegral(0, x - c * qx[qi], phi3) 

            / ( 2.0  *  nu ) );

           vu = bot;
        }
     
     
    delete [] qx;
    delete [] qw;   
    return top / bot;
}





/*


double FF(double x, double t) {
    
    
    int qn = 8;
     double *qw;
     double *qx;
     double vu;
     double nu = 0.5;
     double c;
     double top = 0, bot = 0; 
     qx = new double[qn];
     qw = new double[qn];
     int qi;

     hermite_ek_compute ( qn, qx, qw );

    for ( qi = 0; qi < qn; qi++ )
        {
          c = 2.0 * sqrt ( nu * t);


          


          top = top - qw[qi]  * c * sin (M_PI * (x - c * qx[qi])) 
            * exp ( - cos ( M_PI * (  x - c * qx[qi]  )) 
            / ( 2.0 * M_PI * nu ) );

          bot = bot + qw[qi] * c 
            * exp ( - cos( M_PI * (x - c * qx[qi] ))
            / ( 2.0 *  M_PI * nu ) );

          vu = top;
        }
     
     
     
    delete [] qx;
    delete [] qw;   
    return top / bot;
}

*/




/*
double FF(double x, double t) {
     int qn = 8;
     double *qw;
     double *qx;
     double vu;
     double nu = 1;
     double c; 
     double top = 0, bot = 0; 
     qx = new double[qn];
     qw = new double[qn];
     int qi;
     hermite_ek_compute ( qn, qx, qw );

    for ( qi = 0; qi < qn; qi++ )
        {
          c = 2.0 * sqrt ( nu * t);

                  
          top = top + 4 * qw[qi] * qx[qi] 
            * exp ( -getIntegral(0, x - c * qx[qi], phi3) 
            / ( 2.0 * nu ) );

          bot = bot + qw[qi] * c 
            *  exp ( -getIntegral(0, x - c * qx[qi], phi3) 
            / ( 2.0 * nu ) );

           vu = bot;
        }
    

    return INT3 / vu;

}
*/


double FFF(double x, double t) {
      double nu = 1;
      double sigma2 = 0.001;
      double xMin = -2.0;
      double xMax = 3.0;
      int Nx = 70000;
      double Hx = (xMax - xMin) / Nx;
      double Heta = Hx / 10;
      double Heta1 =  Heta / 10;
      double etaMin = x - 5.0;
      double etaMax = x + 5.0;
      int Netta = floor((etaMax - etaMin) / Heta);
      double fInt3 = 0.0;
      double fInt2 = 0.0;
      for(int j = 1; j < Netta + 1; j++) {   
          double eta = etaMin + (j - 0.5) * Heta;
	  double eta1Min = 0.0;
          double eta1Max = eta;
	  int Neta1 = floor((eta1Max -eta1Min)/Heta1);
          double fInt1 = 0.0;
          for(int k= 1; k < Neta1 + 1; k++) {
	      double eta1 = eta1Min + (k - 0.5) * Heta1;
              fInt1 += exp(-pow(eta1-nu, 2) / (2.*sigma2))/(sqrt(sigma2 * 2 * M_PI)) * Heta1;
          }
	  fInt2 += exp((-pow(x - eta, 2) / (2.0 * t)- fInt1) / 2.0 ) * Heta;
          fInt3 += ((x - eta) / t) * exp((-pow(x-eta, 2) / (2.0 * t)- fInt1) /2.0)*Heta;
      }	  
      return fInt3 / fInt2;
}





double F(double x, double t) {
    double vx[1];
    double vt[1];
    vx[0] = x;
    vt[0] = t;  
    double *res  = burgers_solution (1, 1, vx, 1, vt); 
    double r = res[0]; 
    delete[] res;
    return r;
}



